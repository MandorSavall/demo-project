const {model} = require("mongoose");
const taskSchema = require("../schemas/task");

module.exports = model("Task", taskSchema);
const {Schema} = require("mongoose");

const taskSchema = new Schema({
    text: {
        type: String,
        required: true,
        minLength: 2
    },
    status: {
        type: String,
        required: true,
        enum: ["done", "in-progress"],
        default: "in-progress"
    }
});

module.exports = taskSchema;
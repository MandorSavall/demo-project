const Task = require("../../models/Task");

module.exports = (app) => {
    app.get("/tasks", async (req, res) => {
        try {
            const tasks = await Task.find({});
            res.send({
                status: "Success",
                result: tasks
            });

        } catch (err) {
            res.send({
                status: "Error",
                message: err.message
            })
        }
    });
};
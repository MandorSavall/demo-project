const Task = require("../../models/Task");

module.exports = (app) => {
    app.post("/tasks", async ({body}, res) => {
        const newTask = new Task(body);
        try {
            const task = await newTask.save();
            res.send({
                status: "Success",
                result: task
            });

        } catch (err) {
            res.send({
                status: "Error",
                message: err.message
            })
        }
    });
};
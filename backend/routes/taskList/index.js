const getAllTasks = require("./getAllTasks");
const addTask = require("./addTask");
const removeTask = require("./removeTask");

module.exports = (app) => {
    getAllTasks(app);
    addTask(app);
    removeTask(app);
}
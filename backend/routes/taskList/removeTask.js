const Task = require("../../models/Task");

module.exports = (app) => {
    app.delete("/tasks/:id", async (req, res) => {
        const {id} = req.params;

        try {
            const answer = await Task.findByIdAndDelete(id);
            res.send({
                status: "Success",
                result: answer
            });

        } catch (err) {
            res.send({
                status: "Error",
                message: err.message
            })
        }
    });
};
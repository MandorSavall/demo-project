import BaseHttpService from '../../../services/base-http.service';

export default class ToDoService extends BaseHttpService {
    _endpoint = "tasks";

    async getAllTasks() {
        const data = await this.get(this._endpoint);
        return data;
    }

    async addTask({text, status = "in-progress"}){
        const newTask = {text, status};
        const data = await this.post(this._endpoint, newTask);
        return data;
    }

    async removeTask(_id){
        const data = await this.delete(`${this._endpoint}/${_id}`);
        return data;
    }
}
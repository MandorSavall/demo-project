import React from 'react';

import './ToDoListPage.scss';
import ToDoList from "../../components/ToDoList";

const ToDoListPage = () => {
    return (
        <div>
            <h1>Список дел на завтра</h1>
            <ToDoList />
        </div>
    );
};

export default ToDoListPage;
import React, {useState, useEffect} from 'react';
import {v4} from "uuid";
import ajaxInstance from "../../../../shared/ajax/ajax";

import FormAddTask from "../FormAddTask";
import TaskList from "../TaskList/TaskList";

import ToDoService from "../../services/todo.service";

import './ToDoList.scss';

const service = new ToDoService();

const ToDoList = () => {
    const [list, setList] = useState([]);

    useEffect(async ()=>{
        const data = await service.getAllTasks();
        setList(data.result);
        // ajaxInstance.get("/tasks")
        //     .then(({data}) => setList(data.result))
        //     .catch(err => console.log(err));
    }, []);

    const addTask = async (text)=> {
        const newTask = {
            text,
            status: "in-progress"
        };
        const data = await service.addTask(newTask);
        if(data.status === "Success") {
            setList([...list, data.result]);
        }
    }

    const removeTask = async (_id, idx)=> {
        const data = await service.removeTask(_id);
        if(data.status === "Success") {
            // const newList = [...list];
            // newList.splice(idx, 1);
            const newList = [...list.slice(0, idx), ...list.slice(idx + 1)];
            setList(newList);
        }
    };

    const toggleTaskStatus = (idx)=> {
        const newList = list.map(item => ({...item}));
        const task = newList[idx];
        const newStatus = (task.status === "done") ? "in-progress" : "done";
        newList[idx].status = newStatus;
        setList(newList);
    };

    const listActions = {
        removeTask,
        toggleTaskStatus
    };

    return (
        <div className="tasker">
            <FormAddTask addTask={addTask} />
            <TaskList list={list} {...listActions} />
        </div>
    );
};

export default ToDoList;
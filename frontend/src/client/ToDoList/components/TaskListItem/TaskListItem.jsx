import React from 'react';

import './TaskListItem.scss';

const TaskListItem = ({text, status, removeTask, toggleTaskStatus}) => {
    const statusClassName = (status === "done") ? "completed" : "";
    return (
        <li className={`task ${statusClassName}`}>
            <input type="checkbox" onChange={toggleTaskStatus} className="task-status-change" />
            {text}
            <button className="task-remove" onClick={removeTask}>
                <i className="fa fa-trash"></i>
            </button>
        </li>
    );
};

export default TaskListItem;
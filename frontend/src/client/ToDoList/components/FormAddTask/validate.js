export const validate = (values) => {
    const errors = {};

    if (!values.taskName) {
        errors.taskName = 'Please enter a task';
    }

    return errors;
};
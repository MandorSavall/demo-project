import React from 'react';
import {Formik, Form, Field} from "formik";

import FieldErrorMsg from "../../../../shared/components/FieldErrorMsg";

import './FormAddTask.scss';

import {validate} from "./validate";
import {initialValues} from "./initialValues";

const FormAddTask = ({addTask}) => {

    return (
        <Formik initialValues={initialValues}
                validate={validate}

              onSubmit={ (values, {resetForm}) => {
                addTask(values.taskName);
                  resetForm(initialValues);
              }}>
            <Form>
                <FieldErrorMsg name="taskName" />
                <div className="tasker-header">
                    <Field type="text" name="taskName" placeholder="Enter a task" className="tasker-field" />
                    <button type="submit" className="tasker-button">
                        <i className="fa fa-fw fa-plus"></i>
                    </button>
                </div>
            </Form>

        </Formik>
    );
};

export default FormAddTask;
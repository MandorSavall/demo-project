import React from 'react';

import TaskListItem from "../TaskListItem";

import './TaskList.scss';

const TaskList = ({list, removeTask, toggleTaskStatus}) => {

    const listElements = list.map(({_id, ...taskProps}, index) => <TaskListItem key={_id}
        {...taskProps}
        removeTask={() => removeTask(_id, index)}
        toggleTaskStatus={()=> toggleTaskStatus(index)} />);

    return (
        <div>
            <ul className="tasker-list">
                {listElements}
            </ul>
        </div>
    );
};

export default TaskList;
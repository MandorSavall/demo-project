import React from 'react';
import {Switch, Route} from "react-router-dom";

import ToDoListPage from "./client/ToDoList/pages/ToDoListPage";

const AppRoutes = () => {
    return (
        <Switch>
            <Route path="/" exact>
                <ToDoListPage />
            </Route>
        </Switch>
    );
};

export default AppRoutes;